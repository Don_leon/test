using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Voyages: Entities
    {
        public List<Voyage> GetVoyages(string sorting, string ASKorDESK, Voyage filterA, Voyage filterB, int count, int page)
        {
            List<Voyage> all = GetList<Voyage, Voyage>(sorting, ASKorDESK, filterA, filterB, count, page);
            return all;
        }
        public Voyages()
            : base()
        {

        }
    }
    public class Voyage : Entity
    {
        public Client Client;
        public DateTime DateFly;
        public int Discount;
        public int PrePay;
        public int Price;
        public bool Status;
        public Tour Tour;
        public User User;
        public Voyage(string id, string active, string name, string client, string datefly, string discount, string prepay, string price, string status, string tour, string user)
            : base(int.Parse(id), name, bool.Parse(active))
        {
            Voyages all = new Voyages();
            this.Client = (Client)all.FindById<Client>(int.Parse(client));
            this.Tour = (Tour)all.FindById<Tour>(int.Parse(tour));
            this.DateFly = DateTime.Parse(datefly);
            this.Discount = int.Parse(discount);
            this.PrePay = int.Parse(prepay);
            this.Price = int.Parse(price);
            this.Status = bool.Parse(status);
            this.User = (User)all.FindById<User>(int.Parse(user));

        }
        public Voyage()
            : base()
        {

        }
    }
}
